﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NFourIntegracao.Ioc
{
    public class NativeInjector
    {
        public static void RegisterServices(IServiceCollection services)
        {
            RegisterRepositories(services);
            RegisterDbScripts(services);
        }

        private static void RegisterRepositories(IServiceCollection services)
        {

        }

        private static void RegisterDbScripts(IServiceCollection services)
        {

        }
    }
}
